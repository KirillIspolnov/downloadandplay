package org.kllbff.mdownloader;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Main {
    private static HashMap<String, byte[]> types;
    
    static {
        types = new HashMap<>();
        /*
         * первые несколько байт файла называются "магическими числами"
         * они обозначают тип содержимого
         * какие байты определяют конкретный тип узнать легко - возьмите 3-4 файла одного типа
         * откройте их в HEX-редакторе и посмотрите первые 3-4 байта каждого файла
         * Они будут одинаковыми во всех файлах - это и есть магические числа 
         */
        types.put("mp3", new byte[]{ 0x49, 0x44, 0x33 });
        types.put("png", new byte[]{ (byte)0x89, 0x50, 0x4E });
        types.put("gif", new byte[]{ 0x47, 0x49, 0x46 });
        types.put("jpg", new byte[]{ (byte)0xFF, (byte)0xD8, (byte)0xFF });
    }
    
    /**
     * Cсылка на веб-страницу
     */
    private static final String WEB_PAGE_LINK = "https://zvon.top";
    
    public static void main(String[] args) {
        System.out.printf("Обращаюсь к %s...\n", WEB_PAGE_LINK);
        ArrayList<String> imagesLinks = extractLinks(WEB_PAGE_LINK, "\\s*(?<=data-url\\s?=\\s?\")[^>\"]+", 5);
        ArrayList<String> musicLinks = extractLinks(WEB_PAGE_LINK, "\\s*(?<=<img [^>]{0,100}src\\s?=\\s?\")[^>\"]+", 5);
        
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(() -> downloadFiles(imagesLinks, 1));
        executor.execute(() -> downloadFiles(musicLinks, 1));
        executor.shutdown();
    }
    
    /**
     * Возвращает список найденных на вебстранице совпадений с регулярным выражением<br>
     * В список будет включено первые limit совпадений.
     * 
     * @param webPageLink ссылка на веб-страницу
     * @param regExp регулярное выражение
     * @param limit максимальное количество ожидаемых совпадений
     * @return список найденных на вебстранице совпадений с регулярным выражением
     */
    public static ArrayList<String> extractLinks(String webPageLink, String regExp, int limit) {
        ArrayList<String> storage = new ArrayList<>();
        String webPageContent = readWebPage(webPageLink);
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(webPageContent);
        int count = 0;
        while(matcher.find() && count < limit) {
            storage.add(castToCorrectURL(webPageLink, matcher.group()));
            count++;
        }
        return storage;
    }
    
    /**
     * Возвращает абсолютный адрес, построенный на основе адреса страницы и 
     * относительной ссылки, извлеченной с этой вебстраницы <br />
     * 
     * А зачем это надо?<br />
     * Порой разработчикам при создании сайтов удобнее использовать короткие ссылки
     * на изображения и другой контент. Так, у нас есть страница http://zvon.top,
     * на ней есть картинка logo.png, находящаяся в папке img. Абсолютный адрес,
     * с помощью которого мы можем эту картинку скачать выглядит так:
     * http://zvon.top/img/logo.png. Однако, для удобства, разработчик сайта использует ее 
     * относительный адрес: /img/logo.png. Именно поэтому, нам необходимо построить из
     * относительного адреса абсолютный   
     * 
     * @param page адрес страницы
     * @param link относительная ссылка, найденная на вебстранице
     * @return абсолютный адрес, построенный на основе адреса страницы и относительной ссылки, извлеченной с этой вебстраницы
     */
    
    public static String castToCorrectURL(String page, String link) {
        /*
         * если ссылка начинается с протокола, то ее обрабатывать нам не нужно 
         */
        if(link.startsWith("http")) return link;
        /*
         * Пример: /img/logo.png
         * Такая ссылка указывает, что в корневой папке сервера находится папка img,
         * в которой лежит файл logo.png
         * Это относительная ссылка 
         */
        if(link.startsWith("/")) {
            try {
                /*
                 * Нам лень вручную парсить ссылку page, извлекая из нее протокол,
                 * имя хоста, порта и тому подобную нужную информцию
                 */
                URL pageURL = new URL(page);
                /*
                 * А у класса URL уже есть нужные нам методы
                 */
                return pageURL.getProtocol() + "://" + pageURL.getHost() + link;
            } catch(MalformedURLException murle) {
                murle.printStackTrace();
            }
        }
        if(!page.endsWith("/")) {
            page += "/";
        }
        return page + link;
    }
    
    /**
     * Возвращает содержимое вебстраницы
     *  
     * @param webPageLink ссылка на вебстраницу
     * @return содержимое вебстраницы (HTML-код)
     */
    
    public static String readWebPage(String webPageLink) {
        try(InputStreamReader streamReader = new InputStreamReader(new URL(webPageLink).openStream());
            BufferedReader reader = new BufferedReader(streamReader)) {
            StringBuilder builder = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                builder.append(line).append("\r\n");
            }
            return builder.toString();
        } catch(IOException ioe) {
            ioe.printStackTrace();
            return null;
        }
    }
    
    /**
     * Загружает limit файлов из списка. Если limit > кол-ва ссылок в списке,
     * будут загружены только доступные в списке ссылки
     * 
     * @param links список ссылок на файлы
     * @param limit желаемое кол-во файлов
     */
    public static void downloadFiles(ArrayList<String> links, int limit) {
        for(int i = 0; i < limit && i < links.size(); i++) {
            URL url;
            try {
                url = new URL(links.get(i));
            } catch(MalformedURLException murle) {
                murle.printStackTrace();
                continue;
            }
            
            downloadOneFile(url);
        }
    }
    
    /**
     * Загружает один файл, находящийся по ссылке url
     * Теоритечески, этому методу пофигу какой файл вы скачиваете,
     * он все равно попробует определить его тип САМ
     * 
     * @param url ссылка на файл
     */
    public static void downloadOneFile(URL url) {
        File file = new File("music", url.getFile());
        file.getParentFile().mkdirs();
        String type = null;
        
        try(BufferedInputStream in = new BufferedInputStream(url.openStream());
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
            byte[] buffer = new byte[2048];
            int count = in.read(buffer);
            
            /* Foo-method
             * Это фу-метод. Я так надеялся на Java NIO, думал хорошие дядьки
             * написали мне метод, который смотрит магические числа файла и 
             * определяет по ним тип файла. Шишь! Этот всего лишь определяет
             * тип файла по его расширению!  
             * 
             * type = Files.probeContentType(file.toPath());
             */
            /**
             * А вот это не фу-метод. Я его сам написал)
             * Ыыыы)
             */
            type = probeContentType(buffer);
            do {
                out.write(buffer, 0, count);
            } while((count = in.read(buffer)) != -1);
            
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        
        if(type != null) {
            type = "." + type;
            if(!file.getName().endsWith(type)) {
                File newFile = new File(file.getAbsolutePath() + type);
                file.renameTo(newFile);
                file = newFile;
            }
        }
        
        System.out.println("Готово: " + url + "\n\t" + file);
        
        play(file);
    }
    
    /**
     * Воспроизводит музыкальный файл file
     * 
     * @param file экземпляр класса File, связанный с файлом на диске
     */
    public static void play(File file) {
        try (FileInputStream inputStream = new FileInputStream(file.getAbsolutePath())) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Пробует определить тип содержимого файла. Обычно для определения типа файла
     * достаточно 3-4 байта. Есть, конечно, исключения, (формат pdf, к примеру, или docx/xlsx),
     * но нас это не касается - мы качаем музыку и картинки
     *  
     * @param buffer первые как минимум 3-4 байта. (на самом деле их тут будет аж 2048)
     * @return нужное расширение файла
     */
    public static String probeContentType(byte[] buffer) {
        /*
         * Перебираем типы данных из HashMap с магическими числами 
         */
        for(String type : types.keySet()) {
            boolean equal = true;
            /* магические числа */
            byte[] mask = types.get(type);
            for(int i = 0; i < mask.length; i++) {
                /* каждый байтик сравниваем, заботливо и аккуратно */
                equal &= (buffer[i] == mask[i]);
            }
            if(equal) {
                //ахахаха, подходит, бинго!
                return type;
            }
        }
        //ну нет так нет
        return null;
    }
}
